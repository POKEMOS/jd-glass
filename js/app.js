var box = $('.box').height();
$(function(){
  $('.img_box').height(box + 50);
  $('.img_box_two').height(box + 80);
});

$('.toggle-nav').click(function(){
            
           $('#index_top').stop().slideToggle();
        });

jQuery(function($){
   $("#phone").mask("8(999) 999-9999");

});
$(function() {
                var boxes = [],
                    els, i, l;
                if (document.querySelectorAll) {
                    els = document.querySelectorAll('a[rel=simplebox]');
                    Box.getStyles('simplebox_css', 'simplebox.css');
                    Box.getScripts('simplebox_js', 'simplebox.js', function() {
                        simplebox.init();
                        for (i = 0, l = els.length; i < l; ++i)
                            simplebox.start(els[i]);
                        simplebox.start('a[rel=simplebox_group]');
                    });
                } 
            });
$(document).ready(function(){
	$(".nav").on("click","a", function () {
		var id  = $(this).attr('href'),
			top = $(id).offset().top;
		$('body,html').animate({scrollTop: top}, 1500);
	});
});

function initMap() {
         var moto = {
            lat: 55.809554, lng: 37.794095};
          //Pictures
          var motoimg = './img/logo_mini.png';
    
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: moto,
        });
        var marker = new google.maps.Marker({
          position: moto,
          map: map,
          draggable: true,
          animation: google.maps.Animation.DROP,
          icon: motoimg
        });   
         marker.addListener('click', toggleBounce); 
      }
    function toggleBounce() {
      if (marker.getAnimation() !== null) {
        marker.setAnimation(null);
      } else {
        marker.setAnimation(google.maps.Animation.BOUNCE);
      }
    }